﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContextHelper : MonoBehaviour
{
    public static ContextHelper Instance;

    TextMeshProUGUI TMP;
    // Start is called before the first frame update
    void Awake()
    {
        TMP = GetComponent<TextMeshProUGUI>();
        TMP.enabled = false;
        Instance = this;
    }

    public void DisplayHelp(bool state, string help = null)
    {
        TMP.enabled = state;
        TMP.text = help;
    } 

    public void Clear()
    {
        DisplayHelp(false);
    }
}

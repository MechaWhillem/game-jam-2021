﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum CameraMode
{
    FPS,
    PointAndClic
}

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;

    public float CameraMoveDuration = 1f;
    public CameraMode ActualMode;

    Camera cam;
    FpsInput fpsInput;
    FirstPersonAIO fpsAIO;
    PointAndClicInput pointAndClic;
    
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        cam = Camera.main;
    }

    private void Start()
    {
        fpsInput = GetComponent<FpsInput>();
        fpsAIO = GetComponent<FirstPersonAIO>();
        pointAndClic = GetComponent<PointAndClicInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchCameraMode(CameraMode mode, Vector3 translate, Quaternion rotate, bool NoEnable = false)
    {
        ActualMode = mode;
        
        GetComponent<Rigidbody>().velocity = Vector3.zero;

        //On désactive tout interaction
        fpsInput.enabled = false;
        fpsAIO.enabled = false;
        pointAndClic.enabled = false;

        ContextHelper.Instance.Clear();

        StartCoroutine(MoveCamera(mode, translate, rotate,NoEnable));

        
    }
    
    IEnumerator MoveCamera(CameraMode mode, Vector3 translate, Quaternion identity, bool NoEnable = false)
    {
        if (mode == CameraMode.FPS)
        {
            cam.transform.DOLocalMove(translate, CameraMoveDuration);
            cam.transform.DOLocalRotateQuaternion(identity, CameraMoveDuration);
            Cursor.lockState = CursorLockMode.Locked; Cursor.visible = false;
        }
        else if(mode == CameraMode.PointAndClic)
        {
            cam.transform.DOMove(translate, CameraMoveDuration);
            cam.transform.DORotateQuaternion(identity, CameraMoveDuration);
            Cursor.lockState = CursorLockMode.None; Cursor.visible = true;
        }

        //ATTENTE
        yield return new WaitForSeconds(CameraMoveDuration);

        if (!NoEnable)
        {
            if (ActualMode == CameraMode.FPS)
            {
                fpsInput.enabled = true;
                fpsAIO.enabled = true;
            }
            else if (ActualMode == CameraMode.PointAndClic)
            {
                pointAndClic.enabled = true;
            }
        }
    }

    public void EnableInteraction(bool state)
    {
        pointAndClic.Inactive = state;
    }

}

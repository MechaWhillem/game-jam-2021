﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using NaughtyAttributes;


public class InteractableObject : MonoBehaviour
{
    public Transform CamTarget;
    [ResizableTextArea]
    public string InformationInteraction;
    public bool ZoomIn;
    public UnityEvent OnClicFps;

    Collider col;
    // Start is called before the first frame update
    void Awake()
    {
        FpsInput.OnInteract += Interact;
        FpsInput.OnOver += Over;
        FpsInput.OnEndOver += EndOver;
        PointAndClicInput.OnReturnFPS += Return;

        col = GetComponent<Collider>();
    }

    private void OnDestroy()
    {
        FpsInput.OnInteract -= Interact;
        FpsInput.OnOver -= Over;
        FpsInput.OnEndOver -= EndOver;
        PointAndClicInput.OnReturnFPS -= Return;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Interact(GameObject obj)
    {
        if (obj == gameObject)
        {
            if (ZoomIn)
            {
                CameraManager.Instance.SwitchCameraMode(CameraMode.PointAndClic, CamTarget.position, CamTarget.rotation);
                col.enabled = false;
                if (GetComponent<BlinkingEmissive>() != null)
                {
                    GetComponent<BlinkingEmissive>().Clear();
                    Destroy(GetComponent<BlinkingEmissive>());
                }
            }

            OnClicFps.Invoke();
            ContextHelper.Instance.DisplayHelp(false);
        }
    }

    void Over(GameObject obj)
    {
        if(obj == gameObject)
        {
            //Afficher l'aide en HUD
            ContextHelper.Instance.DisplayHelp(true, InformationInteraction);
        }
    }

    void EndOver()
    {
        
            //Afficher l'aide en HUD
            ContextHelper.Instance.DisplayHelp(false);
       
    }

    public void ForceFps()
    {
        CameraManager.Instance.SwitchCameraMode(CameraMode.FPS, Vector3.zero, Quaternion.identity);
    }

    void Return()
    {
            col.enabled = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InventoryHide : MonoBehaviour
{

    public static InventoryHide Instance;
    Transform parent;
    Vector3 StartPos;
    public float duration;
    Tween tween;


    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent;
        StartPos = transform.position;

        transform.position = parent.position;
    }

    private void OnDestroy()
    {
        tween.Kill();
    }

    public void HideInventory(bool state)
    {
        if (state)
        {
            tween = transform.DOMove(parent.position, duration);
        }
        else
        {
            tween = transform.DOMove(StartPos, duration);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

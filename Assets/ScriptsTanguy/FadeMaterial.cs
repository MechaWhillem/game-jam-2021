﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class FadeMaterial : MonoBehaviour
{
    public UnityEvent EndFading;

    private void Start()
    {
        GetComponent<Renderer>().material.DOFade(0f,0f);
    }

    public void Fading()
    {
        StartCoroutine(DoFade());
    }

    IEnumerator DoFade()
    {
        yield return new WaitForSeconds(1f);
        yield return GetComponent<Renderer>().material.DOFade(1f, 2f).SetLoops(2,LoopType.Yoyo).WaitForCompletion();
        EndFading.Invoke();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PointAndClicInput : MonoBehaviour
{
    public static UnityAction OnReturnFPS;
    public bool Inactive = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1) && Inactive)
        {

            CameraManager.Instance.SwitchCameraMode(CameraMode.FPS, Vector3.zero, Quaternion.identity);
            OnReturnFPS();
        }
    }

    private void OnEnable()
    {
        if(InventoryHide.Instance !=null)
        InventoryHide.Instance.HideInventory(false);
        Inactive = true;
    }

    private void OnDisable()
    {
        InventoryHide.Instance.HideInventory(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlinkingEmissive : MonoBehaviour
{
    public Color BlinkingColor;
    public float duration;

    string colID = "_EmissionColor";
    MeshRenderer rend;
    Color baseColor;
    Coroutine coroutine;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<MeshRenderer>();
        baseColor = rend.material.GetColor(colID);
        coroutine = StartCoroutine(Blinking());
    }

    // Update is called once per frame
    public void Clear()
    {
        StopCoroutine(coroutine);
        rend.material.DOColor(baseColor, colID, duration);
    }

    IEnumerator Blinking()
    {
        while (true)
        {
            rend.material.DOColor(BlinkingColor, colID, duration);
            yield return new WaitForSeconds(duration);
            rend.material.DOColor(baseColor, colID, duration);
            yield return new WaitForSeconds(duration);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FpsInput : MonoBehaviour
{
    public Camera cam;
    [Range(0, 10)]
    public float RayDistance;
    public static UnityAction<GameObject> OnOver;
    public static UnityAction OnEndOver; 
    public static UnityAction<GameObject> OnInteract;
    

    int layerMask = 1 << 8;
    GameObject OverObj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.TransformDirection(Vector3.forward), out hit, RayDistance))
        {

            if (hit.transform.GetComponent<InteractableObject>() != null)
            {
                if (OverObj != hit.transform.gameObject)
                {
                    OverObj = hit.transform.gameObject;
                    Debug.Log(OverObj.name);
                    OnOver(OverObj);
                }
                Debug.DrawRay(cam.transform.position, cam.transform.TransformDirection(Vector3.forward) * hit.distance, Color.blue);
                if (Input.GetMouseButtonDown(0))
                {
                    OnInteract(hit.transform.gameObject);
                }
            }
            else
            {
                    OnEndOver();
                    OverObj = null;
                    //Debug.Log("Did not Hit");
            }

        }
        else
        {
            Debug.DrawRay(cam.transform.position, cam.transform.TransformDirection(Vector3.forward) * RayDistance, Color.white);
            
            if (OverObj != null)
            {
                OnEndOver();
                OverObj = null;
                Debug.Log("Did not Hit");
            }
        }
    }

}

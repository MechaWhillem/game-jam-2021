﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;



public class Inventory : MonoBehaviour
{
    public static Inventory Instance;

    public static ObjectData ActualObj;

    public ObjectData StartObj;

    [SerializeField]
    private Image Img_Slot;

    private void Awake()
    {
        Instance = this;
        Img_Slot.gameObject.SetActive(false);
        UpdateSlot(StartObj);
    }


    public void UpdateSlot(ObjectData objNew)
    {

        if(objNew == null)
        {
            Debug.Log("Not Okay");
            objNew = null;
            ActualObj = null;
        }
        else if (objNew.name == string.Empty)
        {
            Debug.Log("Not Okay");
            objNew = null;
            ActualObj = null;
        }
        else if (objNew.spr == null)
        {
            Debug.Log("Not Okay");
            objNew = null;
            ActualObj = null;
        }
        else
        {
            Debug.Log("Okay");
            ActualObj = objNew;
        }

            if (objNew == null)
            {
                Img_Slot.sprite = null;

                Img_Slot.gameObject.SetActive(false);
                return;
            }
            else
            {
                Img_Slot.sprite = ActualObj.spr;

                Img_Slot.gameObject.SetActive(true);
            }

        }
    }

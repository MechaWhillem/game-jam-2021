﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class InteractionSimple : MonoBehaviour
{
    public UnityEvent OnInteractSimple;
    
    private void OnMouseDown()
    {
        OnInteractSimple.Invoke();
    }
}

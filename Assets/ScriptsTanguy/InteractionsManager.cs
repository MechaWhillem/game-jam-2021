﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class InteractionsManager : MonoBehaviour
{

    int Steps;
    public int StepsMax;
    public UnityEvent OnCompleteInteractions;

    private void Start()
    {
        //StepsMax = transform.childCount;
    }

    public void ValidStep()
    {
        Steps++;
        Debug.Log("Step"+Steps);

        if(Steps == StepsMax)
        {
            StartCoroutine(DoComplete());
        }
    }

    IEnumerator DoComplete()
    {
        yield return new WaitForSeconds(1f);

        OnCompleteInteractions.Invoke();
    }
}

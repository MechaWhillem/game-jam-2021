﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour
{
    public UnityEvent EndOfFade;

    public GameObject TimeDisplay;
    public bool StartOnStart = false;
    [ShowIf("StartOnStart")]
    public DialogueSentences dialog;

    Animator Anim;

    private void Awake()
    {
        Anim = GetComponent<Animator>();
    }

    private void Start()
    {
        if (StartOnStart == true)
        {
            dialog.StartDialogue();
        }
    }

    [Button("Start Fade")]
    public void StartFadeIn()
    {
       StartCoroutine(FadeIn());
    }

    public void StartFadeOut()
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
        Anim.Play("Fade In");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(0.7f);
        TimeDisplay.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Anim.Play("Fade Out");
        EndOfFade.Invoke();
    }

}

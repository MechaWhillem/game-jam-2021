﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;
using UnityEngine.Events;

public class Dialog : MonoBehaviour
{
    public UnityEvent FinishDialogue;

    public static Dialog Instance;

    private TextMeshProUGUI DisplayText;
    [ResizableTextArea]
    private string[] sentences;
    private int index;
    public float TypingSpeed;

    public float WaitTime = 3f;
    private float MaxWaitTime;

    private Coroutine TypeCorou;
    private Coroutine SentenceCorou;
    //Take the Text 
    private void Awake()
    {
        Instance = this;
        MaxWaitTime = WaitTime;
        DisplayText = GetComponent<TextMeshProUGUI>();
    }

    //Start the Dialogue when the object is active
    public void StartType(string[] DisplaySentences)
    {
        sentences = DisplaySentences;
        CameraManager.Instance.EnableInteraction(false);
        TypeCorou = StartCoroutine(Type());
    }

    //Check when the current sentences is finish to activate the button
    private void Update()
    {
        /*if (sentences != null && Input.GetMouseButtonDown(0) && DisplayText.text == sentences[index])
        {
            NextSentence();
        }*/

    }

    //take itch letter of the sentences and display it in the text whitch certain amout of time
    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            
            DisplayText.text += letter;
            yield return new WaitForSecondsRealtime(TypingSpeed);
            CameraManager.Instance.EnableInteraction(false);
        }
        yield return new WaitForSeconds(2f);
        NextSentence();
    }

    //Play the next sentences or return the end of the action if all sentences are done
    IEnumerator NextSentences()
    {
        //play sound here
        DisplayText.gameObject.GetComponent<Animator>().Play("End Text");
        yield return new WaitForSeconds(1f);
        DisplayText.gameObject.GetComponent<Animator>().Play("Start Text");


        
        if (index < sentences.Length - 1 )
        {
            index++;
            DisplayText.text = "";
            TypeCorou = StartCoroutine(Type());
            Debug.Log("Next");
            //yield return new WaitUntil(()=> DisplayText.text == sentences[index]);
            
        }
        else
        {
            DisplayText.text = "";
            FinishDialogue.Invoke();
            sentences = null;
            index = 0;
            CameraManager.Instance.EnableInteraction(true);
            Debug.Log("End");
            CameraManager.Instance.SwitchCameraMode(CameraMode.FPS, Vector3.zero, Quaternion.identity);
            PointAndClicInput.OnReturnFPS();
        }
    }

    public void NextSentence()
    {
        
        SentenceCorou =  StartCoroutine(NextSentences());
        
    }

}

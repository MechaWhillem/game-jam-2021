﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class DialogueSentences : MonoBehaviour
{
    [ResizableTextArea]
    public string[] sentences;

    [Button("Start Dialogue")]
    public void StartDialogue()
    {
        //CameraManager.Instance.EnableInteraction(false);
        Dialog.Instance.StartType(sentences);
    }
}

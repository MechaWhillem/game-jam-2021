﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimStart : MonoBehaviour
{
    public string AnimName;

    Animator Anim;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        float startPoint = Random.Range(0f, 1f);
        Anim.Play(AnimName, -1, startPoint);
    }


}

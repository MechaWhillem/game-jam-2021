﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class start : MonoBehaviour
{
    private bool starts = false;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && starts == false)
        {
            GetComponent<Animator>().Play("End");
            StartCoroutine(next());
            starts = true;
        }
    }

    IEnumerator next()
    {
        yield return new WaitForSeconds(2.2f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
